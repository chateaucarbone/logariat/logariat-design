#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main(){
    ofGLFWWindowSettings settings;
    settings.setSize(640, 1080);
    settings.decorated = false; // No border
	settings.windowMode = OF_WINDOW;
	ofCreateWindow(settings);
	ofRunApp(new ofApp());
}
