# Logariat script

## Contexte

Malgré quelques protestations, un nouveau cadre juridique européen entoure l'utilisation et la sécurisation des données. Les travailleurs qui utilisent des périphériques numériques doivent s'identifier suivant un nouveau protocole (data2citizen, qui inclue leurs données biologiques (signature cardiaque, doigts/geste, voix). Les raisons sont multiples : homogénéisation de la sécurisation des données personnelles et des entreprises, nouvelle norme de santé avec l'apport des données biologiques pour anticiper les arrêts de travail, améliorer le bien-être), nouveaux outils de gestion énergétiques (utilisation de l'eau, de l'oxygène, de l'électricité, ...) et nouveaux outils de traçabilité, d'évaluation (augmentation du télétravail).

Face aux difficultés de mise en œuvre de ces nouvelles normes, les entreprises ont dû se tourner vers d'autres entreprises spécialisées, aidées par des fonds européen de transition. De nouvelles entreprises ont su s'imposer comme leader de ce nouveau marché, notamment **BIOLOG**. Certains pensent que ces entreprises ont su faire preuve d'un lobbying intensif à Bruxelles pour imposer ces coûteux mécanismes.

En parallèle, une avancée notable fait que les données appartiennent à une personne. Chacun peut gérer et vendre ses données sur les sites gouvernementaux des pays respectifs. Mais la plupart utilisent des plateformes public-privé qui offrent plus de possibilité et souvent de meilleurs rendements/revenus. Ce Data Market permet notamment de passer par des courtiers de données et de choisir quelle type de donnée, vendre à qui (Etat, employeur, assurance, recruteurs, supermarché, géants du Web, Banque, Immobilier, commerçants classiques, écoles, ...) ou donner à d'autres personnes, pour sa retraite, à des associations de solidarité. L'Etat taxe sur toutes ces transactions un petit montant pour financer la transition écologique, en relation avec l'empreinte carbone de chaque personne.


## Personnages

### BIOLOG
Entreprise créée par ... (passif des fondateurs).
Histoires ?

BIOLOG produit et vend des équipements d'identifications, dont les fameuses combinaisons. Elle offre des services de mise en place du système **BIOLOG OS**, la gestion, la sécurité.
BIOLOG est aussi connu pour avoir ouvert la voie à l'exploitation d'une région de l'antarctique pour héberger une grosse partie de ces serveurs, suite à un vide juridique ou pour faire face tout simplement à l'augmentation des données.

## MBOCC
Administrateur système au chômage, ancien infirmier.
2 enfants, divorcé.
Accepte une mission de 6 mois très bien payé (6000€/m) pour BIOLOG, dans des conditions difficiles
(~ plateforme pétrolière).


## Mission de MBOCC
Clause de confidentialité (mort sur les réseaux si pas respecté).
Formation d'un mois et suivi psychologique avant de partir
Rôle : Administration des datacenter BIOLOG WORLD et BIOLOG AUTH, 24/24, 7/7



## SCÉNARIO (en cours)
? autre personnage. Ou bien le personnage est vraiment une ombre qui fait le bruitage, l'ambiance
? les personnages se relaient pour travailler 24/24
? L'ANOMALIE


1
sur le porte manteau, un manteau très chaud
mbocc dort sur lit de camp avec couverture
bruits de vent / noise. micro.
bruits de draps
le bruiteur : effet d'ombre / silhouette


2
intro texte blanc sur noir
Article II-127a.
Depuis l'accord-cadre data2citizen, l'identification biologique est obligatoire sur les appareils numériques. Ces données sont un droit monnayable et transitent via les serveurs du groupe
BIOLOGtm en Antarctique.

"En antarcique, les serveurs de données de BIOLOG recueillent et authentifient les données biologiques de millions de citoyens européens". ou plus mystérieux "En antarcique" Il ne faut pas trop en dire à mon avis sur l'extérieur,c'est comme un huit-clos. C'est peut-être le son qui donnera ces informations : machines, vents, etc.


3
costume s'allume suivant un code et mbocc se réveille en urgence mbocc écrit sur le téléphone BioLog OS. Log. Commentaire sur l'anomalie. "Sommeil interrompu par un signal d'erreur de classe A. Comme d'hab. Encore ce rêve bizarre où des choses semblent former un paysage blanc et inquiétant. Vivement sortir de ce trou pour retrouver les filles! smiley " lancement du téléphone au Bruiteur/anomalie qui le regarde, prend le temps de le lire.


4
Dahsboard BIOLOG OS.
mbocc s'authentifie (de façon plus longue et méthodique). Données Bio.
Analyse en cours.
Votre niveau de sommeil/santé est inférieur à 20. Si vous poursuivez, vous perdrait 120 crédits.
Acceptez ?
Votre capital restant est de 1450 crédits.
la session de travail commence
Bruiteur. incarne l'anomalie des données.


5
Se renseigne sur l'anomalie. Lecture d'un message.
Le superviseur automatique a détecté un comportement anormale sur le système d'authentification
BioAuth.
Cohérence des données. Secteur cardiaque. Analyse > diagramme dataviz. Rythmes chaotiques
réguliers deviennent noise.
Log anomalie #Ie130
La cohérence s'est progressivement dégradée jusqu'à arriver sous le seul critique à 02:22:12. Tentative
d'intervention.
Valeurs trop bonnes / normales.
Auto-correction.
Ne marche pas.

6
Correction manuelle.
"Diagnostics" pour réparer.
Mouvements de préparation au travail.
Ouvre Pure Data.
Analyse sonore des données biologiques.
Organes sur le système.
Autre présence. Batterie. > Communication poétique.
Communication. Anomalie.
Canaliser.
La comprendre.
Le blanc recouvre tout
image 3d "nuit polaire"

