# TODO

Développement pour système Debian/Ubuntu.

## Écran 1

### Hard
- Problème avec le partage du flux vidéo de la caméra vers deux applications pour dissocier visualisation HD (web) et tracking (opencv).
- Si pas possible, deux caméras pourraient être nécessaires ? une pour la visualisation HD et l'autre pour le tracking ? 
- Tests de caméras sans filtre IR et projecteur IR. Détection possible avec peinture sur les doigts qui réfléchissent les IR. (skin segmentation, fast.ai)


### Softs

Développement d'un programme pour récupérer, analyser toutes les données entrantes des capteurs (clavier, coeur, tracking 10 doigts) et les envoyer en OSC. 
- Python
- Partage flux caméra : https://github.com/umlaeute/v4l2loopback

Le programme de l'écran 1 doit pouvoir recevoir ces données OSC, afficher deux flux vidéos différents (webcam et wifi cam), une vingtaine de courbes en temps réel. L'image devrait pouvoir réagir rapidement (à la vitesse des frappes), pouvoir être recomposée facilement avec les différents éléments, et pouvoir être traitée avec un ou deux effets vidéos.



| Applications	| Node (ou Meteor ?)				| Natif Linux    					|
| :------------ | :-------------------------------	| :-------------------------------- |
| Caméra 1		| WebRTC, tracking.js, ou emscripten/webassembly ?	| OpenFrameworks (OF) + OpenCV Blob finder ou Python Open CV ?	|
| Caméra 2 Wi-Fi| WebRTC, ?							| OF ?								|
| Courbes		| Canvas/js							| OF
| Traitements vidéo | ?								| OF
| Clavier		| ?									| xinput + python  					|
| Cardiaque     | node-serial, node-midi ou node-osc ?	    	| Arduino serial, PureData comport ou midi ou osc	|
| Micros	    | -									| PureData	 						|




## Écran 2

### Hard
Réfléchir à la possibilité de répartir les charges entre deux ordinateurs. Le portable est l'outil de travail qui se connecte à un serveur, un ordinateur plus puissant qui gère le son et le graphisme. Sur le portable ne serait affiché que l'écran 2, pas les autres écrans. A voir si cela ajoute une couche de complexité ou si c'est cohérent avec le sujet même de la performance, sur le travail à distance (client/serveur cloud).

Routage de l'écran 2 :
- X-forwarding
- Gstreamer
- Node Server
- Répartition écran
- OBS studio


### Softs

| Applications	| Node	(ou Meteor ?)	| Natif Linux      					|
| :------------ | :--------------------	| :-------------------------------- |
| Serveur				| Node					| i3 windows manager (tiling) 		|
| Terminal      | xterm.js, shellinabox, npm xterm-ex	| xcfe4-terminal +  script bash		|
| Éditeur (live)| codemirror, ace		| nano, vim, atom, mousepad	+ LOAF  |
| Pure Data     | noVNC ou ffmpeg + X11grab pour afficher l'interface de Pd sur une page web | Pure Data kiosk plugin |
| Formulaire    | express			 	| Html CSS JS 						|
| Document      | ?      				| Lecteur PDF Atril					|

Se laisser la possibilité d'utiliser un service de live coding pour l'éditeur de texte. Le fait d'écrire des mots dans l'éditeur pourrait générer des effets sur les autres.

!Important: L'écran 2 doit permettre de gérer les fonts, les tailles selon les écrans pour être lisible dans toutes les vidéo projections.


## Écran 3
Réception des données en OSC.

| Applications						| Node	(ou Meteor ?)		| Natif Linux      								|
| :------------ 					| :-----------------------	| :--------------------------------------------	|
| Formes, animations				| Threejs, pixiesjs, p5js 	| OF						 					|
| Bordure de l'écran qui bougent 	| ? shader, screenshots ?	| OF fond transparent ?, shader, screenshot ? 	|


Un fond transparent pourrait être une solution mais comment garder le focus sur l'écran 2 ? Problème sans doute résolu s'il y a deux ordinateurs client/serveur ou deux instances/"tuyaux" graphiques (DISPLAY linux)


