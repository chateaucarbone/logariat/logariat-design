// Get heart sensor to BPM > Serial (byte)

#define USE_ARDUINO_INTERRUPTS true
#include <PulseSensorPlayground.h>

const int PulseWire = 0;
int Threshold = 550;
int myBPM;
PulseSensorPlayground pulseSensor;

void setup() {   
  Serial.begin(9600);
  pulseSensor.analogInput(PulseWire);   
  pulseSensor.setThreshold(Threshold); 
  pulseSensor.begin();
}

void loop() {
  myBPM = pulseSensor.getBeatsPerMinute();
  if (pulseSensor.sawStartOfBeat()) {
   Serial.write(myBPM);
  }
  delay(20);
}