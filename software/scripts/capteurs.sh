#! /bin/bash
killall xinput 2>&1
killall python3 2>&1
killall log_e2_v2 2>&1
killall loaf 2>&1
killall pd 2>&1

# Get absolute path
DIR="$(cd "$(dirname "$0")" && pwd)"

# Heart sensor (Pd patch)
sleep 1

PD_HEART="$DIR/../puredata/_heart.pd"
pd -nogui -noaudio -nomidi -open $PD_HEART 2>&1 &

# Webcam settings
v4l2-ctl --set-ctrl=zoom_absolute=120 -d /dev/video2
v4l2-ctl --set-ctrl=focus_auto=0 -d /dev/video2

# E2
/media/data/softs/of_v0.10.1_linux64gcc6_release/apps/myApps/log_e2_v2/bin/log_e2_v2 &

sleep 1

# E3
/media/data/art/logariat/logariat/software/loaf/loaf /media/data/art/logariat/logariat/software/loaf/e3.lua &

sleep 1

# Keylogger python
xinput --test 13 | python3 $DIR/keyOSC.py 2>&1 &