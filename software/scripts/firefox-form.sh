#!/bin/bash
# from https://unix.stackexchange.com/questions/43106/how-to-set-window-size-and-location-of-an-application-on-screen-via-command-line

URL="file:///media/data/art/logariat/logariat/software/form/index.html"

firefox -new-instance -new-window $URL &

# Process ID of the process we just launched
PID=$!

# Window ID of the process...pray that there's     
# only one window! Otherwise this might break.
# We also need to wait for the process to spawn
# a window.
while [ "$WID" == "" ]; do
        WID=$(wmctrl -lp | grep $PID | cut "-d " -f1)
done
# Set the size and location of the window
# See man wmctrl for more info
wmctrl -i -r $WID -e 0,640,-60,640,1080