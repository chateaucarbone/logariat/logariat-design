#! /bin/bash
# Start Logariat script session

# Clear history
history -c && history -w

# Get absolute path of this script
DIR="$(cd "$(dirname "$0")" && pwd)"

# Scripts list
SCRIPT_COLOR="$DIR/colors.sh"
SCRIPT_CAPTEURS="$DIR/capteurs.sh"
SCRIPT_FORM="$DIR/firefox-form.sh"
SCRIPT_PD="$DIR/pd.sh"

# Activer le script des couleurs
source $SCRIPT_COLOR

function debut_titre(){
	clear
	echo -e $NORMAL
	echo -e "##########################"
	echo -e "#### Logarien.Session ####"
	echo -e "##########################"
	DATE=$(date +%A" "%d.%m.%y" "%R)
	#echo -e "$DATE"
	echo -e "jeudi 14.04.19 18:52"
	echo
	echo -e -n "$WHITE_B\bIdentifiant:$NORMAL "
	read LOGIN
	echo -e -n "$WHITE_B\bMot de passe:$NORMAL "
	read -s PASS
	echo
	echo -e "### Pour l'aide tapez help ###"
	echo 
	echo -e "Bonjour $BOLD$LOGIN$NORMAL, vous êtes dorénavant connecté."
	echo -e "Vous aller encore réaliser de magnifiques tâches"
	echo -e "Nous vous souhaitons un agréable travail."
	echo
} 


function messages(){
	echo
	echo -e "$WHITE_B\bDe:$NORMAL Administrateur"
	echo
	echo -e "$WHITE_B\bDate:$NORMAL 2019.04.04 17:05:32"
	echo
	echo -e "$WHITE_B\bCorps:$NORMAL Les rapports d'hier ne permettent pas de remédier au problème survenus sur nos serveurs. Vous devez nettoyer la base utilisateurs en prenant les précautions nécessaires. Si vous avez besoin d'aides, contactez l'assistance."
	echo
} 


function connect_capteurs(){
	# Erreur Capteur
	echo
	echo -e "$RED_B[Erreur : 303]$NORMAL"
	echo -e "Les capteurs ne soient pas encore activés."
	echo -e "Veuillez les activer conformément à la loi \ncadre relative à la santé."
	echo -e "$GRAY\bArticle 102.38.1$NORMAL"
	echo

	echo -e -n "Entrez le code pour activer les capteurs : "
	read CAPTEUR
	echo
	echo -e "Connection en cours..."
	$SCRIPT_CAPTEURS &	
}

function progress(){
	echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;
}

debut_titre
sleep 1
SPEED=0.5
progress
connect_capteurs

clear

# Invite de commandes
# Boucle infinie qui s'arrête avec break
while [ 1 ]; do
	echo -n -e "$BOLD$LOGIN@logarien$NORMAL$ " 
read reps

function ligne() {
	echo -n -e "\t$BLACK_B\b[$1]$NORMAL $2 \n"
}

function menu_help(){
	ligne "help" "Menu d'aide"
	ligne "about" "Description du programme"
	ligne "M" "Messages reçus"
	ligne "F" "Tâches à faire aujourd'hui"
	ligne "E" "Éditer le fichier README.md"
	ligne "C" "Ouvrir le fichier classeur"
	ligne "T" "Accéder au Terminal distant"
	ligne "S" "Démarrer le serveur audio"
	ligne "L" "Rédiger un texte de loi"
	ligne "Q" "Répondre au questionnaire"
}

function menu_tasks(){
	ligne "nettoyer" "Nettoyer la base utilisateurs logWorld"
	ligne "maintenance" "Le comparateur audio nécessite une correction"
	ligne "maj" "Mettre à jour le système"
	ligne "val" "Valider les décisions"
	ligne "verifier" "Vérifier les enregistrements"
}

# Commandes
case $reps in
  taches | tasks | todo | do | F)
     echo -e "Que souhaitez-vous faire aujourd'hui ?"
     menu_tasks ;;
  help | hlp )
     menu_help ;;
  Q )
  	 killall firefox
     $SCRIPT_FORM & 
     ;;
  M )
     messages
     ;;
  nettoyer )
     echo -e -n "Êtes-vous certain ? [o/n]"
     read YES
     case $YES in
     	o | Y | yes | oui )
     		echo -e "$WHITE_B\b[Info] Passage en mode administrateur$NORMAL"
     		echo -e "sudo clean --force logworld"
     		echo
     		SPEED=0.5
     		progress
     		progress
     		SPEED=0.2
     		progress
     		echo
     		cp "$DIR/message.txt" .
     		vim message.txt
     		echo
     		echo -e -n "Envoyer ? [o/n] : "
     		read ENVOI_MSG
     		SPEED=0.3
     		progress
     		echo
     		;;
     	n | N | no | non)
     	   	echo -e "$RED_B[Erreur : 101.B]$NORMAL"
     		echo -e -n "Nous avons détecté une défaillance d'éxecution."
     		echo -e -n "Envoyer le rapport de logs à l'administration."
     		;;
     esac
     ;;
  maj )
     echo -e "Mise à jour déjà effectuée."
     ;;
  val )
     echo -e "Décisions reportées. Se référer au groupe de discussions de l'agenda."
     ;;
  verifier | check )
     echo -e "Vous n'avez pas les droits. Contactez le service."
     ;;
  E )
     vim README.md &
     ;;
  C )
     echo -e "Vous n'avez pas les droits. Contactez le service comptabilité."
     ;;
  T )
    echo -e "Vous n'avez pas les droits. Contactez le service système."
    ;;
  S | maintenance )
     $SCRIPT_PD & 
     ;;
  L )
  	echo -e "Vous n'avez pas les droits. Contactez le service associé à la rédaction des cadres juridiques du groupe."
    ;;
  about | --v  )
     echo -e "Programme de connection aux logs."
     ;;
  clear | vider | vide )
     clear
     ;;
  quit | "exit" )
     echo -e "Déconnexion du serveur."
     break;;
  * )
    echo -e "Commande inconnue...";;
esac
done
