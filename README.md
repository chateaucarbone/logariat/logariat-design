# Logariat

Résidence de création sur les écritures numériques à la Rochelle

## Écritures numériques
Logariat, c’est l’envie d’explorer les relations :
- entre le travail sur ordinateur, (automatisations, données) et le corps (temps biologique, position assise)
- entre l'écriture (de commandes, de programmes, de lois) et la réalité physique (signaux).

## Performance

![logariat](dessins/logariat.jpg "Log")

Une silhouette humaine noire est assise sur une chaise désagréable. Des lignes lumineuses dessinent son squelette. Un clavier est posé devant, un écran de lumière représente celui d’un ordinateur. Seule action physique, il écrit sur un clavier. 


![logariat2](dessins/log04.png "Log2")

La vidéo projection est découpée en trois zones, un triptyque écriture, corps et images. La zone d'écriture est plutôt austère, sous la forme d'un terminal ou de formulaires. Le corps est représenté par les signaux physiques temps-réel provenant de capteurs sur le corps du performeur. La troisième zone laisse place à la génération de formes organiques entre le camouflage, la cicatrice et des formes artificielles de modélisations physiques de trajectoires, densité, mondes microscopiques, etc.