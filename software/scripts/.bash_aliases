# Logariat cd
alias cdev="cd /media/data/art/logariat/dev/"
alias csoft="cd /media/data/art/logariat/logariat/software/"
alias cscripts="cd /media/data/art/logariat/logariat/software/scripts"

# Logariat launch
alias e1="/media/data/art/logariat/logariat/software/scripts/e1.sh &"
alias capteurs="/media/data/art/logariat/logariat/software/scripts/capteurs.sh &"
alias serveur="/media/data/art/logariat/logariat/software/scripts/pd.sh &"
alias formulaire="/media/data/art/logariat/logariat/software/scripts/firefox-form.sh &"
alias reglementation="firefox /media/data/art/logariat/logariat/data/textes.pdf  &"
alias stop="/media/data/art/logariat/logariat/software/scripts/stop.sh"