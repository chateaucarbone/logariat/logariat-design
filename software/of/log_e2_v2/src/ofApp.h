#pragma once

#include "ofMain.h"
#include <deque>
#include "ofxOsc.h"
#include "ofxOpenCv.h"
#include "ofxHistoryPlot.h"

#define PORT 9000

class ofApp : public ofBaseApp{
	public:

		void setup();
		void update();
		void draw();

		ofxOscReceiver receiver;
				
		ofVideoGrabber cam1;
		ofxCvColorImage img1;
		bool bLearnBackground;
		int vRotation=90;
		bool kOn = false;
		
		ofxHistoryPlot * plot;
		ofxHistoryPlot * plot2;
		float heartData, keyData;
		int plotHeight = 150;
		
		ofTrueTypeFont font;
		string sTime;
};
