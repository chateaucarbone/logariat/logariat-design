s = ""
x=0
y=30
font = of.TrueTypeFont()
nbFrame = 0
speed=10
k=0

function setup()
	of.background(0)
	of.setWindowShape(640, 1080)
	of.setWindowPosition(1280,0)
	of.background(255)
	of.setBackgroundAuto(false)
	of.enableAlphaBlending()
	of.enableAntiAliasing()
	of.setFrameRate(30)
    font:load("fonts/frabk.ttf", 29)
	loaf.setListenPort(9000)
	loaf.startListening()
	of.setColor(80,80,80,20)
end

function update()
	 if ( (nbFrame % speed) == 0) then
	 	y=y+1
	 end
	 nbFrame = nbFrame + 1
end

function draw()
	font:drawString(s,x,y)
end

function oscReceived(message)
	if message:getAddress() == "/speed" then
		speed = message:getArgAsFloat(0)
	elseif message:getAddress() == "/key" then
		--k=1000
		--k = message:getArgAsFloat(0)
		--if ( of.toInt(k) < 255) then
		--	s=string.char(key%255)
		--else 
			s="[]"
		--end
		x=(x+20) % of.getWindowWidth()
		if (y > (of.getWindowHeight() - 30) ) then
			y=30
		end
	end
end
