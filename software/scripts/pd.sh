#! /bin/bash

# Get absolute path
DIR="$(cd "$(dirname "$0")" && pwd)"

# Patch names
PD_MAIN="$DIR/../puredata/main.pd"
PD_BLANK="$DIR/../puredata/_blank.pd"

rm $PD_MAIN
cp $PD_BLANK $PD_MAIN

# Start pd main
pd -nomidi -open $PD_MAIN 2>&1 &

sleep 1