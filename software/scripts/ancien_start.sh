#! /bin/bash
# Broadcast = 127.255.255.255:9000

# Kill all processus
killall pd 2>&1
killall xinput 2>&1
killall python3 2>&1
killall loaf 2>&1
killall log_e2_v2 2>&1

# Webcam settings
v4l2-ctl --set-ctrl=zoom_absolute=120 -d /dev/video2
v4l2-ctl --set-ctrl=focus_auto=0 -d /dev/video2

# Webcam E2
/media/data/softs/of_v0.10.1_linux64gcc6_release/apps/myApps/log_e2_v2/bin/log_e2_v2 &

# Get absolute path
DIR="$(cd "$(dirname "$0")" && pwd)"

# Patch names
PD_MAIN="$DIR/../puredata/main.pd"
PD_BLANK="$DIR/../puredata/_blank.pd"
PD_HEART="$DIR/../puredata/_heart.pd"

rm $PD_MAIN
cp $PD_BLANK $PD_MAIN

# Start pd main
pd -nomidi -open $PD_MAIN 2>&1 &

sleep 1

# Start pd heart sensor serial
pd -nogui -noaudio -nomidi -open $PD_HEART 2>&1 &

sleep 1

# Start keylogger python
xinput --test 13 | python3 $DIR/keyOSC.py 2>&1 &