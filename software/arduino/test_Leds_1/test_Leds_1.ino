#include <FastLED.h>

#define NUM_LEDS 8
#define DATA_PIN 6
CRGB leds[NUM_LEDS];

void setup() {
   	delay(3000);
   	FastLED.addLeds<WS2812, DATA_PIN, RGB>(leds, NUM_LEDS);
   	//FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop() {
   for(int whiteLed = 0; whiteLed < NUM_LEDS; whiteLed = whiteLed + 1) {
      leds[whiteLed] = CRGB::White;
      FastLED.show();
      delay(100);
      leds[whiteLed] = CRGB::Black;
   }
}
