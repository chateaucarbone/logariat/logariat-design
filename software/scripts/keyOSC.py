#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Keylogger > OSC
# Récupère les touches du clavier (sans focus) et envois en OSC
# usage :  xinput --test 13 | python3 keyOSC.py

# Installation : sudo apt install python3 python3-pip python-osc
# ? clean ctrl-C exit: try: + except KeyboardInterrupt:

import sys, socket
from pythonosc import udp_client

if __name__ == "__main__":
		# Broadcast trick
		client = udp_client.SimpleUDPClient("127.255.255.255", 9000)
		client._sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
		# Read standard input (thanks to julien/reso-nance!)
		for line in sys.stdin:
			# If touch is pressed
			# Grab the keycode and send it through UDP/OSC
			if "press" in line:
				keycode = line.rstrip()
				keycode = keycode[-3:]
				client.send_message("/key", int(keycode))
