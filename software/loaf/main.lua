-- J'ai besoin de toi logarien
-- J'en doute

v1 = of.VideoGrabber()
v1_on = true
rotation = 90
color = of.Color(255,255,255)
brightness = 255.0
font = of.TrueTypeFont()

function setup()
	of.background(0)
	of.setWindowShape(640, 1080)
	of.setWindowPosition(640,0)
	of.setWindowTitle("Bienvenue Logarien")
	v1:setDeviceID(0) -- HD Pro Webcam C920
    v1:setDesiredFrameRate(30)
    v1:setup(1024,576) -- 1280,720
    font:load("fonts/frabk.ttf", 17)
	loaf.setListenPort(9999)
	loaf.startListening()
end

function update()
	 v1:update()
end

function draw()
	of.pushMatrix()
	of.translate(v1:getHeight(), 0,0)
	of.rotateZDeg(rotation)
	color:setBrightness(brightness)
	of.setColor(color)
	if v1_on then
		v1:draw(0, 0)
	end
	of.popMatrix()
	
	of.setHexColor(0xFFFFFF)
	font:drawString(tostring(of.getElapsedTimef()), 100, 100)
	
end


function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	--print("script keyPressed: "..tostring(key)
		--.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	--if key == string.byte("s") then
	--	bSmooth = not bSmooth
	--end
	rotation = of.random(45,90)
	brightness = of.random(120,255)
end

















-- OSC
function oscReceived(message)
	if message:getAddress() == "/v1/on" then
		v1_on = message:getArgAsFloat(0)
	elseif message:getAddress() == "/v2/on" then
		v2_on = message:getArgAsFloat(0)
	end
end
