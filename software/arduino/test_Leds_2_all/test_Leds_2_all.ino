#include <FastLED.h>

#define NUM_LEDS 6
#define DATA_PIN 6
CRGB leds[NUM_LEDS];

int brightness = 0;
int fadeAmount = 20;

void setup() {
   	delay(3000);
   	FastLED.addLeds<WS2812, DATA_PIN, RGB>(leds, NUM_LEDS);
    for(int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CRGB::White;
    }
}

void loop() { 
  
  FastLED.setBrightness( brightness );
  FastLED.show();

  brightness = brightness + fadeAmount;
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  
  FastLED.delay(20);  

  if (brightness >= 255) FastLED.delay(4000);  


}
