#include "ofApp.h"

void ofApp::setup(){
	ofSetFrameRate(60);
	ofEnableSmoothing();
	ofEnableAlphaBlending();
	ofSetWindowPosition(0, 0);
	ofSetVerticalSync(true);
	
	bLearnBackground = false;
    cam1.initGrabber(1024,576);
    img1.allocate(1024,576);
    
    ofHideCursor();

	font.load("DejaVuSansMono-Bold.ttf", 20);
	ofSetHexColor(0xffffff);
	
	receiver.setup(PORT);
	
	int numSamples = 350;
	plot = new ofxHistoryPlot(&heartData, "-", numSamples, true);
	plot->setRange(0, plotHeight);
	plot->setDrawGrid(false);
	plot->setColor( ofColor(255,255,255) );
	plot->setBackgroundColor( ofColor(255,255,255,0) );
	plot->setShowNumericalInfo(true);
	plot->setRespectBorders(true);	   //dont let the plot draw on top of text
	plot->setLineWidth(1);
	plot->update(0);
	
	plot2 = new ofxHistoryPlot(&keyData, "-", numSamples, true);
	plot2->setRange(0, plotHeight);
	plot2->setDrawGrid(false);
	plot2->setColor( ofColor(200,200,200) );
	plot2->setBackgroundColor( ofColor(255,255,255,0) );
	plot2->setShowNumericalInfo(true);
	plot2->setRespectBorders(true);	   //dont let the plot draw on top of text
	plot2->setLineWidth(2);
	plot2->update(0);
}

void ofApp::update(){

	// OSC in
	while(receiver.hasWaitingMessages()){
		ofxOscMessage m;
	    receiver.getNextMessage(m);
	    if(m.getAddress() == "/heart/raw"){
	    	heartData = m.getArgAsFloat(0) * plotHeight; // update plot
	    }
	    else if(m.getAddress() == "/key/on"){
	    	if (m.getArgAsFloat(0) > 0) kOn = true; // enable key action
	    	else kOn = false;
	    }
	    else if(m.getAddress() == "/key"){
	    	if (kOn) vRotation=ofRandom(45,90);
	    	keyData = m.getArgAsFloat(0); // update plot
	    }
	}
	
	// Cam
	cam1.update();
    if (cam1.isFrameNew()){
    	img1.setFromPixels(cam1.getPixels());
    }
    
    // Text
    sTime = ofToString(ofGetElapsedTimeMillis());
}


void ofApp::draw(){

	ofBackground(0);
	
	ofPushMatrix();
	ofTranslate(cam1.getHeight(),0,0);
	ofRotateZDeg(vRotation);
	img1.draw(0, 0, ofGetWindowHeight(), ofGetWindowWidth());
	ofPopMatrix();
	
	font.drawString(sTime, 0, 40);
	
	plot->draw(0, (ofGetWindowHeight() - plotHeight) / 2  , ofGetWindowWidth(), plotHeight);
	plot2->draw(0, (ofGetWindowHeight() - plotHeight) / 2  , ofGetWindowWidth(), plotHeight);

}
